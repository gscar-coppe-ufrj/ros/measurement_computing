// usb_1608_fs_plus.h



#ifndef MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_H
#define MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_H



#include <linux/types.h>
#include <iostream>
#include <boost/thread.hpp>
#include <libhid/usb-1608FS-Plus.h>



namespace measurement_computing {



#define MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_INITIAL_RANGE BP_10V
#define MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_SAMPLES 50000
#define MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_FREQUENCY 50000
#define MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_DIGITAL_PERIOD 100



class USB1608FSPlus
{
    bool stopThread;

    usb_dev_handle* udev;
    float tableAIn[NGAINS_USB1608FS_PLUS][NCHAN_USB1608FS_PLUS][2];
    unsigned char ranges[NCHAN_USB1608FS_PLUS];

    usb_dev_handle* USBDeviceFindUSBMCC(int productId);
    virtual void ConnectThread();
    static void ConnectThreadFunction(USB1608FSPlus* component);
    virtual void DigitalDataThread();
    static void DigitalDataThreadFunction(USB1608FSPlus* component);
    virtual void ReadAnalogDataThread();
    static void ReadAnalogDataThreadFunction(USB1608FSPlus* component);
    virtual void SendAnalogDataThread();
    static void SendAnalogDataThreadFunction(USB1608FSPlus* component);

protected:
    int analogSamples;
    double analogFrequency;
    int digitalPeriod;

    bool analogDevicesOn[NCHAN_USB1608FS_PLUS];
    std::string analogDevicesNames[NCHAN_USB1608FS_PLUS];
    double analogDevicesGains[NCHAN_USB1608FS_PLUS];
    unsigned short int analogDevicesValues[NCHAN_USB1608FS_PLUS][MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_SAMPLES];
    double analogDevicesVoltages[NCHAN_USB1608FS_PLUS][MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_SAMPLES];
    unsigned int analogSamplesRead;
    bool analogDataReady;

    bool digitalDevicesOn[NCHAN_USB1608FS_PLUS];
    std::string digitalDevicesNames[NCHAN_USB1608FS_PLUS];
    unsigned char digitalDevicesValues;
    unsigned char digitalDevicesOldValues;

    bool disconnected;
    boost::thread* connectThread;
    boost::thread* digitalDataThread;
    boost::thread* receiveAnalogDataThread;
    boost::thread* sendAnalogDataThread;

    boost::mutex mut;
    boost::mutex mutForCond;
    boost::condition_variable cond;

    void StartCycle();

    virtual void SendAnalogData() = 0;
    virtual void SendDigitalData() = 0;
    void StopThread();

public:
    USB1608FSPlus();
    void FindTools();
    virtual ~USB1608FSPlus();
};



}



#endif // MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_H
