// usb_1608_fs_plus_nodelet.h



#ifndef MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_NODELET_H
#define MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include "usb_1608_fs_plus.h"



namespace measurement_computing {



class USB1608FSPlusNodelet : public USB1608FSPlus, public nodelet::Nodelet
{
    ros::NodeHandle nodeHandle;

    ros::Publisher msgAnalogValuePub[NCHAN_USB1608FS_PLUS];
    ros::Publisher msgAnalogValueVecPub[NCHAN_USB1608FS_PLUS];
    ros::Publisher msgDigitalValuePub[NCHAN_USB1608FS_PLUS];

    void SubscribeAndAdvertiseDevices();

protected:
    void SendAnalogData();
    void SendDigitalData();

public:
    USB1608FSPlusNodelet();
    void onInit();
    virtual ~USB1608FSPlusNodelet();
};



}



#endif // MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_NODELET_H
