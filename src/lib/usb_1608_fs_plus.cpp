// usb_1608_fs_plus.cpp



#include <measurement_computing/usb_1608_fs_plus.h>



measurement_computing::USB1608FSPlus::USB1608FSPlus()
{
    analogSamples = MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_SAMPLES;
    analogFrequency = MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_FREQUENCY;
    digitalPeriod = MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_DIGITAL_PERIOD;

    for (unsigned char i = 0; i < NCHAN_USB1608FS_PLUS; i++)
    {
        analogDevicesNames[i] = "";
        analogDevicesOn[i] = false;
        analogDevicesGains[i] = 0;
    }
    analogSamplesRead = 0;
    analogDataReady = false;

    disconnected = true;
    stopThread = false;
    udev = NULL;
    for (unsigned int i = 0; i < NCHAN_USB1608FS_PLUS; i++)
        ranges[i] = MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_INITIAL_RANGE;
}


void measurement_computing::USB1608FSPlus::StartCycle()
{
    connectThread = new boost::thread(USB1608FSPlus::ConnectThreadFunction, this);
    digitalDataThread = new boost::thread(USB1608FSPlus::DigitalDataThreadFunction, this);
    receiveAnalogDataThread = new boost::thread(USB1608FSPlus::ReadAnalogDataThreadFunction, this);
    sendAnalogDataThread = new boost::thread(USB1608FSPlus::SendAnalogDataThreadFunction, this);
}


usb_dev_handle* measurement_computing::USB1608FSPlus::USBDeviceFindUSBMCC(int productId)
{
    struct usb_bus* bus = NULL;
    struct usb_device* dev = NULL;
    usb_dev_handle* udev = NULL;
    int ret;
    int vendorId = 0x09db; // MCC Vendor ID
    char name[80];

    usb_init();
    //  usb_set_debug(3);
    usb_find_busses();
    usb_find_devices();

    for (bus = usb_get_busses(); bus; bus = bus->next)
    {   // loop through all the busses
        for (dev = bus->devices; dev; dev = dev->next)
        {     // loop through all the devices
            if ( (dev->descriptor.idVendor == vendorId) && (dev->descriptor.idProduct == productId))
            {
                if ((udev = usb_open(dev)))
                {
                    std::cout << "Vendor ID = " << dev->descriptor.idVendor << "    Product ID = " << dev->descriptor.idProduct << std::endl;
                    if (usb_get_driver_np(udev, 0, name, sizeof(name)) == 0)
                    {
                        std::cout << "USB device already bound to driver: " << name << std::endl;
                        usb_close(udev);
                        continue;
                    }

                    /* set the configuration */
                    if ((ret = usb_set_configuration(udev, 1)))
                        std::cerr << "Error setting configuration: " << strerror(errno) << std::endl;
                    /* claim interface */
                    if ((ret = usb_claim_interface(udev, 0)))
                        std::cerr << "Error claiming usb interface 0" << strerror(errno) << std::endl;
                    return udev;
                }
            }
        }
    }
    return NULL;
}


void measurement_computing::USB1608FSPlus::ConnectThread()
{
    while (!stopThread)
    {
        if (disconnected)
        {
            // connection procedure for the DAQ
            if ((udev = USBDeviceFindUSBMCC(USB1608FS_PLUS_PID)) == NULL)
            {
                std::cout << "can't find MCC-USB-1608FS-Plus" << std::endl;
                usleep(1000000);
            }
            else
            {
                std::cout << "MCC-USB-1608FS-Plus found" << std::endl;
                usbBuildGainTable_USB1608FS_Plus(udev, tableAIn);
                disconnected = false;

                usbAInScanStop_USB1608FS_Plus(udev);
                usbAInScanStop_USB1608FS_Plus(udev);
                usbAInScanClearFIFO_USB1608FS_Plus(udev);
                usbAInScanConfig_USB1608FS_Plus(udev, ranges);
                sleep(1);
                usbAInScanConfigR_USB1608FS_Plus(udev, ranges);
            }
        }
        else
            usleep(1000000);
    }
}


void measurement_computing::USB1608FSPlus::ConnectThreadFunction(USB1608FSPlus* component)
{
    component->ConnectThread();
}


void measurement_computing::USB1608FSPlus::DigitalDataThread()
{
    __u8 value = 0;
    bool firstConnection = true;
    while (!stopThread)
    {
        if (disconnected)
        {
            firstConnection = true;
            usleep(100000);
        }
        else
        {
            if (firstConnection)
            {
                usbDTristateW_USB1608FS_Plus(udev, value);
                firstConnection = false;
            }
            digitalDevicesValues = usbDPort_USB1608FS_Plus(udev);

            SendDigitalData();
                digitalDevicesOldValues = digitalDevicesValues;

            usleep(digitalPeriod*1000);
        }
    }
}


void measurement_computing::USB1608FSPlus::DigitalDataThreadFunction(USB1608FSPlus *component)
{
    component->DigitalDataThread();
}


void measurement_computing::USB1608FSPlus::ReadAnalogDataThread()
{
    /*pthread_t pt = receiveThread->native_handle();
    int policy;
    sched_param param;
    //pthread_getschedparam(pt, &policy, &param);
    policy = SCHED_FIFO;
    param.__sched_priority = sched_get_priority_max(SCHED_FIFO);
    pthread_setschedparam(pt, policy, &param);*/

    unsigned char channels = 0;
    unsigned char numberValidChannels = 0;
    for (unsigned int i = 0; i < NCHAN_USB1608FS_PLUS; i++)
    {
        if (analogDevicesOn[i])
        {
            numberValidChannels++;
            channels |= (1 << i);
        }
    }

    __u16 sdataIn[NCHAN_USB1608FS_PLUS*MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_SAMPLES]; // holds 16 bit unsigned analog input data


    while (!stopThread)
    {
        if (disconnected)
            usleep(1000000);
        else
        {
            usbAInScanStart_USB1608FS_Plus(udev, analogSamples, analogFrequency, channels, 0);
            int ret = usbAInScanRead_USB1608FS_Plus(udev, analogSamples, numberValidChannels, sdataIn);
            if (ret < 0)
            {
                disconnected = true;
                std::cout << "DAQ disconnected error:" << ret << std::endl;
                continue;
            }

            mut.lock();
            analogSamplesRead = ret/(2*numberValidChannels);
            if (analogSamplesRead > MEASUREMENT_COMPUTING_USB_1608_FS_PLUS_ANALOG_SAMPLES)
            {
                mut.unlock();
                continue;
            }
            //cout << "Number samples read = " << samplesRead << endl;
            int j = 0;
            for (unsigned int channel = 0; channel < NCHAN_USB1608FS_PLUS; channel++)
            {
                if (analogDevicesOn[channel])
                {
                    for (unsigned int i = 0; i < analogSamplesRead; i++)
                    {
                        analogDevicesValues[channel][i] = rint(sdataIn[i*numberValidChannels + j]*tableAIn[ranges[channel]][channel][0] + tableAIn[ranges[channel]][channel][1]);
                        analogDevicesVoltages[channel][i] = volts_USB1608FS_Plus(udev, analogDevicesValues[channel][i], ranges[channel]);
                        //cout << "Range " << (int)ranges[channel] << " Channel " << (int)channel << " Sample[" << i << "] = " << devicesValues[channel][i] << " Volts = " << devicesVoltages[channel][i] << endl;
                    }
                    j++;
                }
            }
            analogDataReady = true;
            cond.notify_one();
            mut.unlock();
        }
    }
}


void measurement_computing::USB1608FSPlus::ReadAnalogDataThreadFunction(USB1608FSPlus* component)
{
    component->ReadAnalogDataThread();
}


void measurement_computing::USB1608FSPlus::SendAnalogDataThread()
{
    boost::mutex::scoped_lock lock(mutForCond);
    while (!stopThread)
    {
        while (!analogDataReady)
        {
            cond.wait(lock);
            if (stopThread)
                break;
        }
        analogDataReady = false;
        SendAnalogData();
    }
}


void measurement_computing::USB1608FSPlus::SendAnalogDataThreadFunction(USB1608FSPlus* component)
{
    component->SendAnalogDataThread();
}


void measurement_computing::USB1608FSPlus::FindTools()
{
}


void measurement_computing::USB1608FSPlus::StopThread()
{
    stopThread = true;

    if (sendAnalogDataThread != NULL)
    {
        cond.notify_one();
        sendAnalogDataThread->join();
        delete sendAnalogDataThread;
        sendAnalogDataThread = NULL;
    }

    if (receiveAnalogDataThread != NULL)
    {
        receiveAnalogDataThread->join();
        delete receiveAnalogDataThread;
        receiveAnalogDataThread = NULL;
    }

    if (digitalDataThread != NULL)
    {
        digitalDataThread->join();
        delete digitalDataThread;
        digitalDataThread = NULL;
    }

    if (connectThread != NULL)
    {
        connectThread->join();
        delete connectThread;
        connectThread = NULL;

        // disconnect procedure from the DAQ
        if (!disconnected)
            cleanup_USB1608FS_Plus(udev);
    }
}


measurement_computing::USB1608FSPlus::~USB1608FSPlus()
{
    StopThread();
}
