// usb_1608_fs_plus_nodelet.cpp



#include <measurement_computing/usb_1608_fs_plus_nodelet.h>



measurement_computing::USB1608FSPlusNodelet::USB1608FSPlusNodelet()
{
}


void measurement_computing::USB1608FSPlusNodelet::SubscribeAndAdvertiseDevices()
{
    for (unsigned char i = 0; i < NCHAN_USB1608FS_PLUS; i++)
    {
        if (analogDevicesNames[i] != "")
        {
            msgAnalogValuePub[i] = nodeHandle.advertise<std_msgs::Float64>(getName() + "/" + analogDevicesNames[i] + "/AnalogValue", 5, true);
            msgAnalogValueVecPub[i] = nodeHandle.advertise<std_msgs::Float64MultiArray>(getName() + "/" + analogDevicesNames[i] + "/AnalogValueVec", 5, true);
        }
        if (digitalDevicesNames[i] != "")
            msgDigitalValuePub[i] = nodeHandle.advertise<std_msgs::Bool>(getName() + "/" + digitalDevicesNames[i] + "/DigitalValue", 5, true);
    }
}


void measurement_computing::USB1608FSPlusNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    privateNH.param("analog_samples", analogSamples, analogSamples);
    privateNH.param("analog_frequency", analogFrequency, analogFrequency);
    privateNH.param("digital_period", digitalPeriod, digitalPeriod);

    for (unsigned char i = 0; i < NCHAN_USB1608FS_PLUS; i++)
    {
        std::stringstream ss;
        ss << (int)i;
        analogDevicesOn[i] = privateNH.getParam("analog_device/" + ss.str() + "/name", analogDevicesNames[i]);
        if (analogDevicesOn[i])
            privateNH.param("analog_device/" + ss.str() + "/gain", analogDevicesGains[i], 1.0);
        digitalDevicesOn[i] = privateNH.getParam("digital_device/" + ss.str() + "/name", digitalDevicesNames[i]);
    }

    nodeHandle = getNodeHandle();

    SubscribeAndAdvertiseDevices();
    StartCycle();
}


void measurement_computing::USB1608FSPlusNodelet::SendAnalogData()
{
    mut.lock();
    if (!disconnected && analogSamplesRead > 0)
    {
        std_msgs::Float64 msg;
        std_msgs::Float64MultiArrayPtr msgVec(new std_msgs::Float64MultiArray);
        for (unsigned char i = 0; i < NCHAN_USB1608FS_PLUS; i++)
        {
            if (analogDevicesOn[i])
            {
                msgVec->data.resize(analogSamplesRead);
                for (unsigned int j = 0; j < analogSamplesRead; j++)
                    msgVec->data[j] = analogDevicesGains[i]*((double)(analogDevicesVoltages[i][j]));
                msgAnalogValueVecPub[i].publish(msgVec);

                msg.data = analogDevicesGains[i]*((double)(analogDevicesVoltages[i][analogSamplesRead - 1]));
                msgAnalogValuePub[i].publish(msg);
            }
        }
        analogSamplesRead = 0;
    }
    mut.unlock();
}


void measurement_computing::USB1608FSPlusNodelet::SendDigitalData()
{
    std_msgs::Bool msg;
    if (!disconnected)
    {
        for (unsigned char i = 0; i < NCHAN_USB1608FS_PLUS; i++)
        {
            if (digitalDevicesOn[i] && ((digitalDevicesValues >> i) & 1) != ((digitalDevicesOldValues >> i) & 1))
            {
                msg.data = (digitalDevicesValues >> i) & 1;
                msgDigitalValuePub[i].publish(msg);
            }
        }
    }
}


measurement_computing::USB1608FSPlusNodelet::~USB1608FSPlusNodelet()
{
    StopThread();
}
